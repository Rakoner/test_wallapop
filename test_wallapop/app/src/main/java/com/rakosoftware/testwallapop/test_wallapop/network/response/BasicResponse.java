package com.rakosoftware.testwallapop.test_wallapop.network.response;

/**
 * Created by Joss on 17/06/2016.
 */
public class BasicResponse {

    protected int code;
    protected String status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
