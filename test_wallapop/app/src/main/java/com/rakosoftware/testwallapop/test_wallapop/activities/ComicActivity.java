package com.rakosoftware.testwallapop.test_wallapop.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.adapters.ComicPagerAdapter;
import com.rakosoftware.testwallapop.test_wallapop.controller.Controller;
import com.rakosoftware.testwallapop.test_wallapop.utils.ExtraParams;

/**
 * Created by Joss on 26/07/2016.
 */
public class ComicActivity extends Activity {

    private ViewPager mViewPager;
    private ComicPagerAdapter mComicPagerAdapter;
    private int animalPositionSelected;
    private String comicFilterText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);
        Bundle b = getIntent().getExtras();
        animalPositionSelected = b.getInt(ExtraParams.EXTRA_COMIC_POSITION);
        comicFilterText = b.getString(ExtraParams.EXTRA_COMIC_FILTER);
        mComicPagerAdapter = new ComicPagerAdapter(this, Controller.getInstance().getFilteredComics(comicFilterText));

        mViewPager = (ViewPager) findViewById(R.id.pagerComic);
        mViewPager.setAdapter(mComicPagerAdapter);
        mViewPager.setCurrentItem(animalPositionSelected);
    }
}
