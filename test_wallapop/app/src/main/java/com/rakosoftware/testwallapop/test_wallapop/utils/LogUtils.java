package com.rakosoftware.testwallapop.test_wallapop.utils;

import android.util.Log;

import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;

import java.util.List;

/**
 * Created by Chema on 26/07/2016.
 */
public class LogUtils {

    public static void logComics(List<Comic> comicList){
        if(comicList!=null){
            for(Comic comic: comicList){
                Log.i("Comic",comic.toString());
            }
        }
    }

}
