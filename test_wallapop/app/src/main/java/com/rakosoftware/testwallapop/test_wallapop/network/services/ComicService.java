package com.rakosoftware.testwallapop.test_wallapop.network.services;


import com.rakosoftware.testwallapop.test_wallapop.network.NetworkConstants;
import com.rakosoftware.testwallapop.test_wallapop.network.response.GetComicsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Joss on 12/06/2016.
 */
public interface ComicService {

    @Headers("Content-Type: application/json")
    @GET(NetworkConstants.GET_COMICS_URL)
    Call<GetComicsResponse> getComics(@Query(NetworkConstants.WALLAPOP_MARVEL_TS) String ts, @Query(NetworkConstants.WALLAPOP_MARVEL_HASH) String hash);

}
