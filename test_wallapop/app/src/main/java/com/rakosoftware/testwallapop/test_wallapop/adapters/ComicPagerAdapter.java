package com.rakosoftware.testwallapop.test_wallapop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;
import com.rakosoftware.testwallapop.test_wallapop.servicesbeans.ComicBeanService;
import com.rakosoftware.testwallapop.test_wallapop.utils.Constants;
import com.rakosoftware.testwallapop.test_wallapop.utils.ImageUtils;
import com.rakosoftware.testwallapop.test_wallapop.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Joss on 15/06/2016.
 */
public class ComicPagerAdapter extends PagerAdapter {

    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Comic> comicList;
    private boolean isPortrait;

    public ComicPagerAdapter(Activity activity, ArrayList<Comic> comicList) {
        mActivity = activity;
        mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.comicList=comicList;
        isPortrait= Utils.getScreenOrientation(mActivity)== Configuration.ORIENTATION_PORTRAIT;
    }

    @Override
    public int getCount() {
        return comicList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.page_comic, container, false);

        TextView tvComicTitle = (TextView) itemView.findViewById(R.id.tvComicTitle);
        tvComicTitle.setText(comicList.get(position).getTitle());

        TextView tvComicDescription = (TextView) itemView.findViewById(R.id.tvComicDescription);
        tvComicDescription.setText(Html.fromHtml(comicList.get(position).getDescription()));

        ImageView imgComic = (ImageView) itemView.findViewById(R.id.imgComic);
        //imgComic.setScaleType(isPortrait?ImageView.ScaleType.CENTER_CROP:ImageView.ScaleType.CENTER_INSIDE);
        String finalImagePath = ImageUtils.getFinalImageUrl(new ComicBeanService().getRandomImage(comicList.get(position)));
        if(URLUtil.isValidUrl(finalImagePath)){
            ImageUtils.loadImage(mActivity, finalImagePath,imgComic, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);
        }else{
            ImageUtils.loadImage(mActivity, "drawable://" + R.drawable.comic_default_image,imgComic, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}