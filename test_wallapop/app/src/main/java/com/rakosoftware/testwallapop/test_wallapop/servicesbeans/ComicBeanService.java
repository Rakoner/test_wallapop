package com.rakosoftware.testwallapop.test_wallapop.servicesbeans;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.activities.MainActivity;
import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;
import com.rakosoftware.testwallapop.test_wallapop.beans.ImageComic;
import com.rakosoftware.testwallapop.test_wallapop.controller.Controller;
import com.rakosoftware.testwallapop.test_wallapop.network.NetworkConstants;
import com.rakosoftware.testwallapop.test_wallapop.network.NetworkUtils;
import com.rakosoftware.testwallapop.test_wallapop.network.response.GetComicsResponse;
import com.rakosoftware.testwallapop.test_wallapop.network.services.ComicService;
import com.rakosoftware.testwallapop.test_wallapop.network.services.ServiceGenerator;
import com.rakosoftware.testwallapop.test_wallapop.utils.LogUtils;
import com.rakosoftware.testwallapop.test_wallapop.utils.Utils;

import java.util.Collections;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Chema on 25/07/2016.
 */
public class ComicBeanService {

    public void processGetComicsServerCall(GetComicsResponse getComicsResponse){
        Controller.getInstance().setComicList(getComicsResponse.getData().getResults());
        Collections.sort(Controller.getInstance().getComicList());
        LogUtils.logComics(Controller.getInstance().getComicList());
    }

    public void callServerGetComics(final Activity activity){
        ComicService comicsService = ServiceGenerator.createService(ComicService.class, activity,activity.getResources().getString(R.string.loading));
        Call<GetComicsResponse> call = comicsService.getComics("1469478252000","4c8aa3b45c679e1ba6e3f8f8d32a414d");
        call.enqueue(new Callback<GetComicsResponse>() {
            @Override
            public void onResponse(Call<GetComicsResponse> call, Response<GetComicsResponse> response) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onResponse, NetworkConstants.CALL_SERVER_GET_COMICS,activity.getClass().getSimpleName()));
                GetComicsResponse getComicResponse = response.body();
                if(getComicResponse!=null){
                    if(getComicResponse.getCode()==200){
                        processGetComicsServerCall(getComicResponse);
                        ((MainActivity)activity).showComics();
                        NetworkUtils.dismissProgress(activity);
                    }else{
                        NetworkUtils.dismissProgress(activity);
                        Toast.makeText(activity.getApplicationContext(),activity.getResources().getString(R.string.generic_error),Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALL_SERVER_GET_COMICS,activity.getClass().getSimpleName()));
                }
                //((MainActivity)activity).hideSoftKeyboard();
            }

            @Override
            public void onFailure(Call<GetComicsResponse> call, Throwable t) {
                Log.i(activity.getResources().getString(R.string.retrofit_log), activity.getResources().getString(R.string.retrofit_onFailure, NetworkConstants.CALL_SERVER_GET_COMICS,activity.getClass().getSimpleName()));
                Utils.showInfoDialog(activity.getResources().getString(R.string.retrofit_error, NetworkConstants.CALL_SERVER_GET_COMICS), activity);
                System.out.println(call);
                //((MainActivity)activity).hideSoftKeyboard();
            }
        });
    }

    public ImageComic getRandomImage(Comic comic){
        ImageComic result = null;
        if(comic.getImages() !=null){
            Random rn = new Random();
            int pos = rn.nextInt(comic.getImages().size());
            result = comic.getImages().get(pos);
        }
        return result;
    }

}
