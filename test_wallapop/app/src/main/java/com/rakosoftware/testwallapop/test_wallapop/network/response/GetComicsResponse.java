package com.rakosoftware.testwallapop.test_wallapop.network.response;

import com.rakosoftware.testwallapop.test_wallapop.beans.DataComic;

/**
 * Created by Joss on 12/06/2016.
 */
public class GetComicsResponse extends BasicResponse{

    protected DataComic data;

    public DataComic getData() {
        return data;
    }

    public void setData(DataComic data) {
        this.data = data;
    }
}
