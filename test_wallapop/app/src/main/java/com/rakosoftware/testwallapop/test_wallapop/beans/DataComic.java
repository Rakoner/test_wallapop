package com.rakosoftware.testwallapop.test_wallapop.beans;

import java.util.ArrayList;

/**
 * Created by Joss on 26/07/2016.
 */
public class DataComic {

    protected int offset;
    protected int limit;
    protected int total;
    protected int count;
    protected ArrayList<Comic> results;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Comic> getResults() {
        return results;
    }

    public void setResults(ArrayList<Comic> results) {
        this.results = results;
    }

}
