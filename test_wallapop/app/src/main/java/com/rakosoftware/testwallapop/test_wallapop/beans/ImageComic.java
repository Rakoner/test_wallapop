package com.rakosoftware.testwallapop.test_wallapop.beans;

/**
 * Created by Rako on 26/07/2016.
 */
public class ImageComic {

    protected String path;
    protected String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
