package com.rakosoftware.testwallapop.test_wallapop.utils;

import android.content.Context;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.rakosoftware.testwallapop.test_wallapop.beans.ImageComic;

/**
 * Created by Joss on 13/06/2016.
 */
public class ImageUtils {

    public static final String HARDCODED_IMAGE_SIZE="/portrait_incredible.";

    public static void loadImage(Context context, String imageUrl, ImageView imageView, DisplayImageOptions displayImageOptions) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            if (displayImageOptions == null) {
                displayImageOptions = Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS;
            }
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(displayImageOptions)
                    .build();
            imageLoader.init(config);
        }
        imageLoader.displayImage(imageUrl, imageView, displayImageOptions);
    }

    public static String getFinalImageUrl(ImageComic imageComic){
        String result="";
        if(imageComic!=null){
            result=imageComic.getPath()+HARDCODED_IMAGE_SIZE+imageComic.getExtension();
        }
        return result;
    }

}
