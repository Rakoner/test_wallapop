package com.rakosoftware.testwallapop.test_wallapop.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.rakosoftware.testwallapop.test_wallapop.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;


/**
 * Created by Joss on 17/06/2016.
 */
public class NetworkUtils {

    public static void dismissProgress(Context aCtx){
        Utils.cancelProgressDialog(aCtx);
    }

    public static boolean isDeviceOnline(Context aCtx) {
        if(aCtx!=null){
            ConnectivityManager cm =
                    (ConnectivityManager)aCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }else{
            return true;
        }
    }

    public static String getURLGetParams(){
        long timestamp = new Date().getTime();
        return "?"+NetworkConstants.WALLAPOP_MARVEL_APIKEY+
                "&"+NetworkConstants.WALLAPOP_MARVEL_TS+Long.toString(timestamp)+
                "&"+NetworkConstants.WALLAPOP_MARVEL_HASH+getMD5Conversion(Long.toString(timestamp)+NetworkConstants.WALLAPOP_MARVEL_PRIVATE_KEY+NetworkConstants.WALLAPOP_MARVEL_PUBLIC_KEY);
    }

    private static String getMD5Conversion(String origin){
        StringBuffer sb=null;
        try {
            final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(origin.getBytes("UTF-8"));
            sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (final java.security.NoSuchAlgorithmException e) {
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
