package com.rakosoftware.testwallapop.test_wallapop.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.utils.Constants;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        doSplashStuff();
    }

    public void doSplashStuff() {
        //TODO splash things like initialize libraries or download data
        waitAndGo();
    }

    public void waitAndGo() {
        // Execute some code after 2.5 seconds have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goToMainActivity();
            }
        }, Constants.SPLASH_FREEZE_TIME);
    }

    public void goToMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
