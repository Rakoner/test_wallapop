package com.rakosoftware.testwallapop.test_wallapop.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.adapters.ComicAdapter;
import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;
import com.rakosoftware.testwallapop.test_wallapop.controller.Controller;
import com.rakosoftware.testwallapop.test_wallapop.customizedviews.VerticalSpaceItemDecoration;
import com.rakosoftware.testwallapop.test_wallapop.network.NetworkUtils;
import com.rakosoftware.testwallapop.test_wallapop.servicesbeans.ComicBeanService;
import com.rakosoftware.testwallapop.test_wallapop.utils.Constants;
import com.rakosoftware.testwallapop.test_wallapop.utils.ExtraParams;
import com.rakosoftware.testwallapop.test_wallapop.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

/**
 * Created by Rako on 25/07/2016.
 */
public class MainActivity extends Activity {

    private String screenName=this.getClass().getSimpleName();

    private ComicAdapter comicAdapter;

    @Bind(R.id.recViewComics)
    RecyclerView recViewComics;

    @Bind(R.id.tvNoComic)
    TextView tvNoComic;

    @Bind(R.id.edFinder)
    EditText mEdFinder;


    @OnTextChanged(R.id.edFinder)
    protected void onFilterFinderChanged() {
        showComics();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initComics();
        getComics();
    }

    @Override
    public void onResume() {
        super.onResume();
        tvNoComic.requestFocus();
    }

    public void getComics(){
        if(NetworkUtils.isDeviceOnline(MainActivity.this)){
            new ComicBeanService().callServerGetComics(MainActivity.this);
        }else{
            Utils.showInfoDialog(getResources().getString(R.string.internet_no_connection), MainActivity.this);
        }
    }

    public void showComics(){
        ArrayList<Comic> filteredComics = Controller.getInstance().getFilteredComics(mEdFinder.getText().toString());
        if(filteredComics.size()>0){
            recViewComics.setVisibility(View.VISIBLE);
            tvNoComic.setVisibility(View.GONE);
            comicAdapter = new ComicAdapter(filteredComics, MainActivity.this);
            recViewComics.setAdapter(comicAdapter);
            comicAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recViewComics.getChildAdapterPosition(v);
                    goToComicDetail(position);
                }
            });
        }else{
            recViewComics.setVisibility(View.GONE);
            tvNoComic.setVisibility(View.VISIBLE);
        }
    }

    public void initComics(){
        recViewComics.setHasFixedSize(true);
        recViewComics.addItemDecoration(new VerticalSpaceItemDecoration(Constants.VERTICAL_ITEM_SPACE));
        recViewComics.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
    }

    public void goToComicDetail(int comicPosSelected){
        Intent intent = new Intent(this, ComicActivity.class);
        intent.putExtra(ExtraParams.EXTRA_COMIC_POSITION,comicPosSelected);
        intent.putExtra(ExtraParams.EXTRA_COMIC_FILTER,mEdFinder.getText().toString());
        startActivity(intent);
    }


}
