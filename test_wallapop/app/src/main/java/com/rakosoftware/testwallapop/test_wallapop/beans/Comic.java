package com.rakosoftware.testwallapop.test_wallapop.beans;

import java.util.ArrayList;

/**
 * Created by Joss on 26/07/2016.
 */
public class Comic implements Comparable<Comic>{

    protected int id;
    protected String title;
    protected String description;
    protected ImageComic thumbnail;
    protected ArrayList<ImageComic> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImageComic getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageComic thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ArrayList<ImageComic> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageComic> images) {
        this.images = images;
    }

    @Override
    public int compareTo(Comic other) {
        return this.getTitle().compareTo(other.getTitle());
    }

    @Override
    public String toString() {
        return "Comic{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail=" + thumbnail +
                ", images=" + images +
                '}';
    }
}
