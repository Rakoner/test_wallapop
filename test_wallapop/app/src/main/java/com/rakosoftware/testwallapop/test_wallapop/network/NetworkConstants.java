package com.rakosoftware.testwallapop.test_wallapop.network;

/**
 * Created by Joss on 12/06/2016.
 */
public class NetworkConstants {

    //TODO this must be in flavours for int pre pro etc environtments, not mocked like this
    public static final String BASE_URL_SERVER="http://gateway.marvel.com/";
    private static final String AMERICAN_CAPTAIN_ID="1009220";

    public static final String GET_COMICS_URL =BASE_URL_SERVER+"v1/public/characters/"+NetworkConstants.AMERICAN_CAPTAIN_ID+"/comics"+NetworkConstants.WALLAPOP_MARVEL_APIKEY;

    public static final String WALLAPOP_MARVEL_PUBLIC_KEY="6a7ed890b4b941a925202a5630d5b162";
    public static final String WALLAPOP_MARVEL_PRIVATE_KEY="0f1d0fdf46a0bf32f962b0b9997233c0395cdf8e";

    public static final String WALLAPOP_MARVEL_APIKEY="?apikey=6a7ed890b4b941a925202a5630d5b162";
    public static final String WALLAPOP_MARVEL_TS="ts";
    public static final String WALLAPOP_MARVEL_HASH="hash";

    public static final String CALL_SERVER_GET_COMICS="GetComics";

}
