package com.rakosoftware.testwallapop.test_wallapop.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;

import com.rakosoftware.testwallapop.test_wallapop.R;
import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;
import com.rakosoftware.testwallapop.test_wallapop.utils.Constants;
import com.rakosoftware.testwallapop.test_wallapop.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by Rako on 26/07/2016.
 */
public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ComicViewHolder>  implements View.OnClickListener {

    private ArrayList<Comic> comicList;
    private Context mContext;
    private View.OnClickListener listener;

    public ComicAdapter(ArrayList<Comic> comicList, Context aCtx) {
        this.comicList = comicList;
        this.mContext=aCtx;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    @Override
    public ComicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comic, parent, false);
        itemView.setOnClickListener(this);
        return new ComicViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ComicViewHolder holder, int position) {
        Comic item = comicList.get(position);
        holder.bindComic(item,mContext);
    }

    @Override
    public int getItemCount() {
        return comicList.size();
    }

    public static class ComicViewHolder extends RecyclerView.ViewHolder {

        private com.rakosoftware.testwallapop.test_wallapop.customizedviews.ImageViewRoundedFitWidth imgComic;
        private TextView tvComicTitle;

        public ComicViewHolder(View itemView) {
            super(itemView);
            imgComic = (com.rakosoftware.testwallapop.test_wallapop.customizedviews.ImageViewRoundedFitWidth)itemView.findViewById(R.id.imgComic);
            tvComicTitle = (TextView)itemView.findViewById(R.id.tvComicTitle);
        }

        public void bindComic(Comic comic,Context aCtx) {
            tvComicTitle.setText(comic.getTitle());
            String finalImagePath = ImageUtils.getFinalImageUrl(comic.getThumbnail());
            if(URLUtil.isValidUrl(finalImagePath)){
                ImageUtils.loadImage(aCtx, finalImagePath,imgComic, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);
            }else{
                ImageUtils.loadImage(aCtx, "drawable://" + R.drawable.comic_default_image,imgComic, Constants.DEFAULT_DISPLAY_IMAGE_OPTIONS);
            }
        }
    }

}