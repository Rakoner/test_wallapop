package com.rakosoftware.testwallapop.test_wallapop.controller;

import android.content.Context;

import com.rakosoftware.testwallapop.test_wallapop.beans.Comic;

import java.util.ArrayList;

/**
 * Created by Joss on 26/07/2016.
 */
public class Controller {

    private static Controller ourInstance;
    private Context myContext;
    private ArrayList<Comic> comicList;

    public static Controller getInstance() {
        if (ourInstance == null) {
            ourInstance = new Controller();
        }
        return ourInstance;
    }

    public static Controller getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(Controller ourInstance) {
        Controller.ourInstance = ourInstance;
    }

    public Context getMyContext() {
        return myContext;
    }

    public void setMyContext(Context myContext) {
        this.myContext = myContext;
    }

    public ArrayList<Comic> getComicList() {
        return comicList;
    }

    public void setComicList(ArrayList<Comic> comicList) {
        this.comicList = comicList;
    }


    public ArrayList<Comic> getFilteredComics(String filter) {
        ArrayList<Comic> result;
        if(filter.length()==0){
            result = getComicList();
        }else{
            result = new ArrayList<>();
            for(Comic Comic: getComicList()){
                if(Comic.getTitle().toLowerCase().contains(filter)){
                    result.add(Comic);
                }
            }
        }
        return result;
    }
    
}
