package com.rakosoftware.testwallapop.test_wallapop.utils;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.rakosoftware.testwallapop.test_wallapop.R;

/**
 * Created by Rako on 25/07/2016.
 */
public class Constants {

    public static long SPLASH_FREEZE_TIME=3000;
    public static int VERTICAL_ITEM_SPACE=30;


    // Universal Image Loader
    public static DisplayImageOptions DEFAULT_DISPLAY_IMAGE_OPTIONS = new DisplayImageOptions.Builder()
            .cacheOnDisk(true)
            .cacheInMemory(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .showImageForEmptyUri(R.drawable.comic_default_image)
            .showImageOnFail(R.drawable.comic_default_image)
            .build();
}
